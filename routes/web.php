<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['checkrole','auth'])->group(function(){
// });

Route::get('/superadmin', 'SuperAdminController@index');
Route::get('/admin', 'AdminController@index');
Route::get('/guest', 'GuestController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
